const gulp = require('gulp')
const sass = require('gulp-sass')
const clean = require('gulp-clean')
const concat = require('gulp-concat')
const cleanCss = require('gulp-clean-css')
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const mergeMedia = require('gulp-merge-media-queries')
const rename = require('gulp-rename')
const autoprefixer = require('gulp-autoprefixer')
const browserSync = require('browser-sync')


const path = {
    dev: {
        html: 'index.html',
        scripts: 'src/js/**/*.js',
        styles: 'src/scss/**/*.scss',
        images: 'src/img/**/*.{png,jpg,jpeg,gif,svg,mp4,webm}',
        videos: 'src/img/**/*.{mp4,webm}',
        assets: 'src/assets/**/*.ico',
        files: 'src/files/**/*.pdf'
    },
    build: {
        root: 'dist',
        html: 'index.html',
        scripts: 'dist/js',
        styles: 'dist/css',
        images: 'dist/img',
        assets: 'dist/assets',
        files: 'dist/files'
    }
}

const cleanDist = () => (
    gulp.src(path.build.root, {allowEmpty: true})
        .pipe(clean())
)

const buildStyles = () => (
    gulp.src(path.dev.styles)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(mergeMedia())
        .pipe(cleanCss())
        .pipe(rename('styles.min.css'))
        .pipe(gulp.dest(path.build.styles))
)

const buildScripts = () => (
    gulp.src(path.dev.scripts)
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(path.build.scripts))
)

const buildImages = () => (
    gulp.src(path.dev.images)
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.mozjpeg({quality: 75, progressive: true}),
            imagemin.optipng({optimizationLevel: 5})
        ]))
        .pipe(gulp.dest(path.build.images))
)

const buildVideos = () => (
    gulp.src(path.dev.videos)
        .pipe(gulp.dest(path.build.images))
)

const buildFiles = () => (
    gulp.src(path.dev.files)
        .pipe(gulp.dest(path.build.files))
)

const buildAssets = () => (
    gulp.src(path.dev.assets)
        .pipe(gulp.dest(path.build.assets))
)

const serve = () => {
    gulp.watch(path.dev.scripts, buildScripts)
    gulp.watch(path.dev.styles, buildStyles)
    gulp.watch(path.dev.images, buildImages)
    return browserSync.init({
        server: {
            baseDir: './'
        },
        files: [
            {match: path.build.html, fn: this.reload},
            {match: path.build.root, fn: this.reload}
        ]
    })
}

gulp.task('dev', serve)

gulp.task('build', gulp.series(
    cleanDist,
    buildStyles,
    buildScripts,
    buildImages,
    buildVideos,
    buildFiles,
    buildAssets
))