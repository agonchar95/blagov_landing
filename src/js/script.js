//tabs

document.querySelector('.tabs__wrapper').addEventListener('click', tabDisplay);

function tabDisplay(event) {
    if (event.target.getAttribute('data-tab') != null) {
        let dataTab = event.target.getAttribute('data-tab');
        let tabsTitles = document.getElementsByClassName('tabs__header');
        for (let i = 0; i < tabsTitles.length; i++) {
            tabsTitles[i].classList.remove('tabs__header--active');
        }
        event.target.classList.add('tabs__header--active');
        let contentToShow = document.getElementsByClassName('tabs__article');
        for (let i = 0; i < contentToShow.length; i++) {
            if (+dataTab === i) {
                contentToShow[i].style.display = 'block';
            } else {
                contentToShow[i].style.display = 'none';
            }
        }
    }
}


//modals
const submit = document.querySelectorAll('.form__button');
submit.forEach(el => el.addEventListener('click', e => e.preventDefault()));

const playVideoBtn = document.querySelector('.play');
const videoModal = document.querySelector('.iframe');
const modalWrapper = document.querySelector('.modal__wrapper');
const modalClose = document.querySelector('.modal__close');

const openRegBtn = document.querySelectorAll('.open__form');
const openQuestionBtn = document.querySelectorAll('.open__form--question');
const modalFormWrapper = document.querySelector('.modal__form__wrapper');
const modalFormWrapperQuestion = document.querySelector('.modal__form__wrapper--question');
const modalFormClose = document.querySelectorAll('.form__close');


playVideoBtn.addEventListener('click', () => {
    document.body.style.overflow = 'hidden';
    modalWrapper.style.display = 'block';
    let videoSrc = videoModal.getAttribute('data-src');
    videoModal.setAttribute('src', videoSrc + '?autoplay=1&cc_load_policy=1');
    videoModal.setAttribute('allow', 'autoplay');
});

modalClose.addEventListener('click', () => {
    document.body.style.overflow = 'unset';
    modalWrapper.style.display = 'none';
    videoModal.removeAttribute('src');
    videoModal.removeAttribute('allow');
});

openRegBtn.forEach(el => {el.addEventListener('click', () => {
    document.body.style.overflow = 'hidden';
    modalFormWrapper.style.display = 'block';
})});

openQuestionBtn.forEach(el => {el.addEventListener('click', () => {
    document.body.style.overflow = 'hidden';
    modalFormWrapperQuestion.style.display = 'block';
})});

modalFormClose.forEach(el => el.addEventListener('click', (e) => {
    document.body.style.overflow = 'unset';
    modalFormWrapper.style.display = 'none';
    modalFormWrapperQuestion.style.display = 'none';

}));

//burger

const burger = document.querySelector('.burger');
const mobileMenu = document.querySelector('.header--mobile__menu__wrapper');
const menuLinks = document.querySelectorAll('.menu__link');

burger.addEventListener('click', () => {
    burger.classList.toggle('burger--active');
    mobileMenu.classList.toggle('hide');
});

menuLinks.forEach(el => el.addEventListener('click', () => {
    mobileMenu.classList.add('hide');
    burger.classList.remove('burger--active');
}));

//read more

const readMore = document.querySelectorAll('.tabs__article__more');
const texts = document.querySelectorAll('.tabs__article__text');
let state = false;

readMore.forEach(item => {
    item.addEventListener('click', (e) => {
        let i = item.getAttribute('data-article');
        texts[i].classList.toggle('tabs__article__text--active');
        state = !state;
        item.innerText = state ? 'Свернуть' : 'Читать далее...';
        if (texts[i].style.maxHeight) {
            texts[i].style.maxHeight = null;
        } else {
            texts[i].style.maxHeight = texts[i].scrollHeight + "px";
        }
    })
});

//swiper examples
const swiper = new Swiper('.examples__cards__wrapper--mobile', {
    speed: 1000,
    mousewheel: {
        releaseOnEdges: true,
    },
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    scrollbar: {
        el: ".examples__scrollbar",
    },
    allowTouchMove: true,
    touchReleaseOnEdges: true
});

//swiper refs

const swiper2 = new Swiper('.refs__photo__wrapper--mobile', {
    speed: 1000,
    mousewheel: {
        releaseOnEdges: true,
    },
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    scrollbar: {
        el: ".refs__scrollbar",
    },
    allowTouchMove: true,
    touchReleaseOnEdges: true
});

//swiper refs desktop

const swiper3 = new Swiper('.refs__photo__wrapper--desktop', {
    // speed: 1000,
    mousewheel: {
        releaseOnEdges: true,
    },
    scrollbar: {
        el: ".refs__scrollbar",
    },
    slidesPerView: 3,
    spaceBetween: 30,
    freeMode: true,
    allowTouchMove: true,
    touchReleaseOnEdges: true
});